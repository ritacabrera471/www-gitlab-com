---
layout: handbook-page-toc
title: "Sigstr"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Uses

Sigstr is a marketing tool used as an advertising channel inside the inbox. 