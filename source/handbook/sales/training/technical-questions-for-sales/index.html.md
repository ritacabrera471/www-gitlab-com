---
layout: handbook-page-toc
title: "Technical Questions for Sales"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## **Introduction**
To improve sales efficiency and reduce the dependence of GitLab sales team members on Solution Architects (SAs), the SA team created the below table outlining some of the most common technical questions they believe GitLab sales team members should be able to answer. The following questions have been organized by DevOps lifecycle stage as outlined on [this page](/stages-devops-lifecycle/) which describes how DevOps is better with GitLab.

### Manage

#### What authentication options are available when using .com? 

*  **Short answer**: Coming soon
*  **Context video**: 
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8o1Ifdte6Ps" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

*  **Learn more**: Check out the [System for Cross-domain Identity Management (SCIM) documentation](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html)

#### What options do we have to manage user access on .com?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon 
*  **Learn more**: Check out the [SAML SSO for GitLab.com groups documentation](https://docs.gitlab.com/ee/user/group/saml_sso/)

#### How should customers set up and customize user roles in GitLab?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon 
*  **Learn more**: There are multiple types of permissions across GitLab, and when implementing anything that deals with permissions, all of them should be considered. Check out the [GitLab permissions guide](https://docs.gitlab.com/ee/development/permissions.html).

#### How can users be auto-provisioned?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon 
*  **Learn more**: Review the [Create users through integrations documentation](https://docs.gitlab.com/ee/user/profile/account/create_accounts.html#create-users-through-integrations)

#### How is user management different on .com compared to self-managed?
*  **Short answer**: Coming soon
*  **Context video**: Coming soon 
*  **Learn more**: Administrators have more powerful user management capabilities. Check out [all the differences between GitLab.com and self-managed](/handbook/marketing/product-marketing/dot-com-vs-self-managed/#all-differences-between-gitlabcom-and-self-managed).

### Plan

#### What best practices exist around using projects/groups?

*  **Short answer**: Groups are used to organize projects, control permissions to them, and aggregate data across them. 
*  **Context video**: Coming soon 
*  **Learn more**: 
    *  Review the [No Tissues with Issues](/handbook/marketing/product-marketing/getting-started/101/) page
    *  Watch [this video](https://www.youtube.com/watch?v=VR2r1TJCDew) (Jan 2019, 15 minutes)

#### How can we integrate Jira?

*  **Short answer**: Coming soon
*  **Context video** (5 minutes, May 2020): 
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/et3umKvcrX4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

*  **Learn more**: Check out the [GitLab Jira Integration page](/solutions/jira/)  

#### What is the difference between the Jira integration in Starter and in Premium?

*  **Short answer**: Coming soon
*  **Context video** (5 minutes, May 2020):
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/t0_52j2Zp2I" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

*  **Learn more**: Review the [GitLab Jira development panel integration](https://docs.gitlab.com/ee/integration/jira_development_panel.html) documentation

### Create

#### How may customers move from SVN to git?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Read the [Migrating from SVN to GitLab documentation](https://docs.gitlab.com/ee/user/project/import/svn.html)

#### What are best practices for a developer workflow?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Take a look at [Introduction to GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)

#### What are the differences around merge request approvals between the EE tiers?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Check out the [GitLab Create Features page](https://about.gitlab.com/features/#create)

### Verify

#### How does GitLab CI work?
*  **Short answer**: Coming soon
*  **Context video** (9 minutes, May 2020): 
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/bmnFgGSY_L8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

*  **Learn more**: Continuous Integration works by pushing small code chunks to your application’s code base hosted in a Git repository, and, to every push, run a pipeline of scripts to build, test, and validate the code changes before merging them into the main branch. Check out the [GitLab CI/CD documentation](https://docs.gitlab.com/ee/ci/). 

#### How can we do test management with GitLab?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: The GitLab Product Team is exploring enhancements in this area. Check out the [Quality Management Direction page](/direction/plan/quality_management/). 

#### What are runners and how do they actually work?

*  **Short answer**: Coming soon
*  **Context video** (18 minutes, May 2020): 
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IsthhMm64u8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
*  **Learn more**: GitLab Runner is the open source project that is used to run your jobs and send the results back to GitLab. It is used in conjunction with [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/), the open-source continuous integration service included with GitLab that coordinates the jobs. Find out more by reading the [GitLab Runner documentation](https://docs.gitlab.com/runner/).

#### What are best practices for a runner infrastructure? 

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Review the [best practices documentation for using and administering GitLab Runner](https://docs.gitlab.com/runner/best_practice/)

### Package

#### For what purpose or use case may the package repositories in GitLab be used? Will they replace Nexus or Artifactory?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Check out the [JFrog Artifactory vs. GitLab comparison](/devops-tools/jfrog-artifactory-vs-gitlab.html)

### Secure

#### What are the GitLab security capabilities?
*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: GitLab can check your application for security vulnerabilities that may lead to unauthorized access, data leaks, denial of services, and more. Check out the [GitLab Secure documentation](https://docs.gitlab.com/ee/user/application_security/).

#### When will we no longer rely on using Docker-in-Docker (DinD) for security scanners? 

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: This is an ongoing effort. See the [Use non-DinD mode by default for Security Products issue](https://gitlab.com/gitlab-org/gitlab/-/issues/37278) for more information.

### Release

No questions at this time.

### Configure

No questions at this time.

### Monitor

No questions at this time.

### Defend

No questions at this time.

### Enable

#### What are the best ways to achieve high availability?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: View the [GitLab Jira Integration page](/solutions/jira/)

#### What are the best ways to achieve disaster recovery?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Review the [Replication with Geo documentation](https://docs.gitlab.com/ee/administration/geo/replication/index.html)

#### What is GitLab's out-of-the-box support for (AWS/Azure/GCP)?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Please look at GitLab's [multi cloud support](/multicloud/)

#### Does GitLab have reference architectures with sizing information?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: GitLab supports a number of scaling options to ensure that your self-managed instance is able to scale out to meet your organization’s needs when scaling up a single-box GitLab installation is no longer practical or feasible. Check out [GitLab's Reference Architecture documentation](https://docs.gitlab.com/ee/administration/scaling/index.html#reference-architectures).

#### What are the best practices for backing up a GitLab instance?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Check out the [Backing up and restoring GitLab documentation](https://docs.gitlab.com/ee/raketasks/backup_restore.html)

#### What installation method does GitLab recommend customers use?

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: GitLab strongly recommends downloading the Omnibus package installation since it is quicker to install, easier to upgrade, and contains features to enhance reliability not found in other methods. We also strongly recommend [at least 4GB of free RAM](https://docs.gitlab.com/ee/install/requirements.html#cpu) to run GitLab. Find out more on the [GitLab Installation page](/install/). 

#### How do customers migrate from GitLab CE to EE? 

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Check out the [Upgrade to Enterprise Edition page](/upgrade/) 

#### How do customers migrate from GitLab self-managed to .com? 

*  **Short answer**: Coming soon
*  **Context video**: Coming soon
*  **Learn more**: Find out how on the [Upgrade to Enterprise Edition page](https://docs.gitlab.com/ee/user/project/import/#migrating-from-self-managed-gitlab-to-gitlabcom) 
