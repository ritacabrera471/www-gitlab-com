require_relative '../../generators/direction'

describe Generators::Direction do
  context 'GitLabInstance' do
    subject { described_class::GitLabInstance.new('gitlab.com') }

    context '#get' do
      let(:path) { '/projects/1/milestones' }
      let(:url) { 'https://gitlab.com/api/v4/projects/1/milestones?page=1' }
      let(:headers) { { "Content-Type" => "application/json" } }
      let(:payload) { { test: 123 } }

      before do
        subject.cache_store.clear
        stub_request(:get, url).to_return(status: 200, headers: headers, body: payload.to_json)
      end

      it 'caches the path' do
        2.times do
          result = subject.get("projects/1/milestones", true)

          expect(result).to eq('test' => 123)
        end

        expect(WebMock).to have_requested(:get, url).once
      end
    end
  end
end
